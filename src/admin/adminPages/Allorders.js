import { Fragment, useEffect, useState } from "react";
import AllOrdersCard from "../adminComponents/AllOrdersCard";
import { Row, Col, Card } from "react-bootstrap";
import AdminPageAfterLoginViewNav from "../adminNav/AdminPageAfterLoginViewNav"
import Container from 'react-bootstrap/Container';
import AdminFooter from "../AdminFooter"


export default function AllOrders(){

const [orders, setOrders] = useState([]);
const [email, setEmail] = useState("");






useEffect(() => {
	fetch("https://vast-island-07766.herokuapp.com/api/order/users/orders", {
		method: "GET",
		headers: {
			"Content-Type": "application/json",
			Authorization: `Bearer ${localStorage.getItem("token")}`
		}
	})
	.then(res => res.json())
	.then(data => {
		console.log(data)

		

setOrders(data.map(order => {
	return (
	
		

		<AllOrdersCard key={order._id} orderProp={order} />
		
     
	)
})
)





	})
}, []);




	return (
		<Fragment>
		<AdminPageAfterLoginViewNav />
		<Row className="mt-3 mb-3 container-fit justify-content-center">
			
			{orders}
			<h6>Total Amount: {}</h6>
			</Row>

			<AdminFooter />
		</Fragment>
     
		

		)
}