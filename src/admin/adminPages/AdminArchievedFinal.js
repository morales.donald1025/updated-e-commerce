import { useState, useEffect, useContext } from 'react';
import { Form, Button, Container } from 'react-bootstrap';
import { Redirect } from 'react-router-dom';
import AdminArchieved from "./AdminArchieved";
import AdminPageAfterLoginViewNav from "../adminNav/AdminPageAfterLoginViewNav";
import AdminFooter from "../AdminFooter";
import { Fragment } from 'react';


export default function AdminArchievedFinal(){
    return (
        <Fragment>
        <AdminPageAfterLoginViewNav />
        <AdminArchieved />
        <AdminFooter />
        </Fragment>

        )
}