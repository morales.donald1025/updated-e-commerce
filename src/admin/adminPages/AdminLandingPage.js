import React from "react"
import AdminNav from "../adminNav/AdminNav"
import AdminLandingPageComp from "../AdminLandingPageComp"
import AdminFooter from "../AdminFooter"


export default function AdminLandingPage(){
	return(
		<>
		<AdminNav />
		<AdminLandingPageComp />
		<AdminFooter />
		</>
		)
}