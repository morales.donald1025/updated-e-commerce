import AdminPageAfterLoginViewNav from "./adminNav/AdminPageAfterLoginViewNav";
import AdminFooter from "./AdminFooter";
import { Fragment } from 'react';
import AddProducts from "./AddProducts"


export default function AddProductFinal(){
  return (
    <Fragment>
    <AdminPageAfterLoginViewNav />
    <AddProducts />
    <AdminFooter />
    </Fragment>
    )
}