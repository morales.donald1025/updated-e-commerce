import { useState, useEffect, useContext } from 'react';
import { Form, Button, Container } from 'react-bootstrap';
import { Redirect } from 'react-router-dom';
import CartView from "../components/CartView";
import CartNav from "../components/CartNav";
import Footer from "../components/Footer";
import { Fragment } from 'react';


export default function Login(){
    return (
        <Fragment>
        <CartNav />
        <CartView />
        <Footer />
        </Fragment>

        )
}