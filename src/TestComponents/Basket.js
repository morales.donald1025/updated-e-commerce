import React from "react";
import { Row, Col, Card } from "react-bootstrap";
import { useState } from "react";
import { loadStripe } from "@stripe/stripe-js"

import IndexCheckout from "../pages/IndexCheckout"
const stripePromise = loadStripe("pk_test_51KIPpzH68q6QTForzb9SjxyCLfDDSBJb4hTzwRsnKqg2HkFwBCzVoI15OeSFsr9DC86vDXROvqq9csnaqGeDX3qk00DnnauvXv");



export default function Basket(props) {
  const { cartItems, onAdd, onRemove } = props;
  const itemsPrice = cartItems.reduce((a, c) => a + c.quantity * c.price, 0);
  const taxPrice = itemsPrice * 0.14;
  const shippingPrice = itemsPrice > 2000 ? 0 : 20;
  const totalPrice = itemsPrice + taxPrice + shippingPrice;









  return (


<div>
    <Row className="mt-3 mb-3 container-fit justify-content-center">
    
    <Col xs={12} md={12} className="mt-2">
    <Card>

    
      <h2>Cart Items</h2>
      <div>
        {cartItems.length === 0 && <div>Cart is empty</div>}
        {cartItems.map((item) => (
          <div key={item.id} className="row mt-3">
            <div className="col-2"><h6>{item.name}</h6></div>
            <div className="col-2">
              <button onClick={() => onRemove(item)} className="remove">
                -
              </button>{' '}
              <button onClick={() => onAdd(item)} className="add" >
                +
              </button>
            </div>
            <div className="col-3">
              <h6>Quantity: {item.quantity}</h6>
              </div>
               <div className="col-3">
              <h6>Price: PHP {item.price.toFixed(2)}</h6>
              </div>
            
          </div>
        ))}

        {cartItems.length !== 0 && (
          <>
            <hr></hr>
            <div className="row">
              <div className="col-2">Items Price</div>
              <div className="col-1 text-right">${itemsPrice.toFixed(2)}</div>
            </div>
            <div className="row">
              <div className="col-2">Tax Price</div>
              <div className="col-1 text-right">${taxPrice.toFixed(2)}</div>
            </div>
            <div className="row">
              <div className="col-2">Shipping Price</div>
              <div className="col-1 text-right">
                ${shippingPrice.toFixed(2)}
              </div>
            </div>

            <div className="row">
              <div className="col-2">
                <strong>Total Price</strong>
              </div>
              <div className="col-1 text-right">
                <strong>${totalPrice.toFixed(2)}</strong>
              </div>
            </div>
            <hr />
           
          </>
        )}
        <IndexCheckout />
      </div>
    </Card>
       </Col>
    
    </Row>

    </div>
  );
}
