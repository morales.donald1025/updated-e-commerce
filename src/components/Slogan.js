import React from "react"
import { Row, Col, Card } from "react-bootstrap";



export default function Slogan(){
	return (
<Row className = "mt-3 mb-3 container-fit justify-content-center">
			
		<Col xs={12} md={12} >
		<Card className="cardHighlight">
  <Card.Body>
    <Card.Title>
    	<h1 className="text-center">Mathweb Clothing Company</h1>
    </Card.Title>
    <Card.Text className="text-center"><h3>Not just fashion, but design.</h3></Card.Text>
  </Card.Body>
</Card>		
			</Col>
		</Row>
		)
}